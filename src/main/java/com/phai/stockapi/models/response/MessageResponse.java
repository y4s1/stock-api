package com.phai.stockapi.models.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MessageResponse {

    private String code;
    private String message;
    private String messageKh;
    private Object data;

    public void setGetDataSuccess(Object data) {
        this.code = "200";
        this.message = "Get data success";
        this.messageKh = "ជោគជ័យ";
        this.data = data;
    }
}
