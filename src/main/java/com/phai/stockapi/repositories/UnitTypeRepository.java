package com.phai.stockapi.repositories;

import com.phai.stockapi.models.UnitType;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface UnitTypeRepository extends  JpaRepository<UnitType, Integer>{

}
