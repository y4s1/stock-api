package com.phai.stockapi.services.impl;

import com.phai.stockapi.exceptions.ApiException;
import com.phai.stockapi.models.UnitType;
import com.phai.stockapi.repositories.UnitTypeRepository;
import com.phai.stockapi.services.UnitTypeService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UnitTypeServiceImpl implements UnitTypeService {
    private final UnitTypeRepository unitTypeRepository;

    @Override
    public List<UnitType> getAll() {
        return unitTypeRepository.findAll();
    }


    @Override
    public UnitType getById(Integer id) throws ApiException {
        var unitType = unitTypeRepository.findById(id).orElse(null);
        if(unitType == null){
            throw new ApiException("Unit type not found","រកមិនឃើញ","400");
        }

        return unitType;
    }
}
