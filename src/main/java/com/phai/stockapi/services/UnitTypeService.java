package com.phai.stockapi.services;

import com.phai.stockapi.exceptions.ApiException;
import com.phai.stockapi.models.UnitType;

import java.util.List;

public interface UnitTypeService {

    List<UnitType> getAll();

    UnitType getById(Integer id) throws ApiException;
}
