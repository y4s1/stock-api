package com.phai.stockapi.exceptions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class ApiException extends Throwable{


    private String messageKh;
    private String code;


    public ApiException(){
        super();
    }

    public ApiException(String message,String messageKh,String code){
        super(message);
        this.messageKh = messageKh;
        this.code = code;
    }
}
