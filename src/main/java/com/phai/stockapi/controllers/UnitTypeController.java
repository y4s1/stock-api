package com.phai.stockapi.controllers;

import com.phai.stockapi.exceptions.ApiException;
import com.phai.stockapi.models.response.MessageResponse;
import com.phai.stockapi.services.UnitTypeService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
@Slf4j
@RequiredArgsConstructor
public class UnitTypeController {
    private final UnitTypeService unitTypeService;
    private MessageResponse messageResponse;

    @GetMapping("/unitTypes")
    public ResponseEntity<Object> getAll(){
        messageResponse = new MessageResponse();
        messageResponse.setGetDataSuccess(unitTypeService.getAll());
        return new ResponseEntity<>(messageResponse, HttpStatus.OK);
    }

    @GetMapping("/unitTypes/{id}")
    public ResponseEntity<Object> getById(@PathVariable("id") int id){
        messageResponse = new MessageResponse();
        try{
            messageResponse.setGetDataSuccess(unitTypeService.getById(id));

        }catch (ApiException e){
            messageResponse = new MessageResponse(e.getCode(), e.getMessage(),e.getMessageKh(),null);
        }
        return new ResponseEntity<>(messageResponse, HttpStatus.OK);
    }


}
